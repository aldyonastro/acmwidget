# Astro ACM Flutter Widget

With this project, we are aiming to provide default styling widget for all project in Astro based on 
[ACM Style guide](https://app.zeplin.io/project/5ca46235eb413e0ab9bceea7/screen/5cc270bbd576fb3d99b32750) 

## Currently available
* Text Collection
* Button Collection


## Usage
Paste this code to pubspec.yaml 
```
acm_widgets:
    git:
      url: https://bitbucket.org/aldyonastro/acmwidget.git
```

and run yaml `flutter packages get`

For detail documentation clone the project and open /doc/api/index.html
Check and running example project to check widgets on device.

###Text Collections 
![](images/AcmTextExm.png)

```
Import `package:acm_widget/acm_text.dart`
```
####Available widget : 
* [Title Text](doc/api/text/AcmTitleText-class.html)
* [Body Text](doc/api/text/AcmBodyText-class.html)

####[Detail implementation](doc/api/text/text-library.html)

###Button Collections 

![](images/AcmButtonExm.png)

```
Import `package:acm_widget/acm_button.dart`
```
####Available widget : 
* [Primary Button](doc/api/button/AcmPrimaryButton-class.html)
* [Secondary Button](doc/api/button/AcmSecondaryButton-class.html)

####[Detail implementation](doc/api/button/button-library.html)


