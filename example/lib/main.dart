import 'package:example/text_page.dart';
import 'package:flutter/material.dart';
import 'package:acm_widgets/acm_button.dart';

import 'button_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Acm Widgets',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      routes: {
        '/' : (context) => MyHomePage(title: "Acm Widget",),
        ButtonPage.route : (context) => ButtonPage(),
        TextPage.route : (context) => TextPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    print(Offset(0.0, -1.0).distanceSquared - Offset(0.0, 0.0).distanceSquared);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:
      ListView(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  RaisedButton(
                    child: Text("Text Example"),
                    onPressed: () {
                      Navigator.pushNamed(context, TextPage.route);
                    }
                  ),

                  RaisedButton(
                    child: Text("Button Example"),
                    onPressed: () {
                      Navigator.pushNamed(context, ButtonPage.route);
                    }
                  )

                ],
              ),
            ),
          ),
        ],
      )
    );
  }
}