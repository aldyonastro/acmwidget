import 'package:acm_widgets/acm_button.dart';
import 'package:flutter/material.dart';

class ButtonPage extends StatefulWidget {

  static const String route = "/buttonexample";

  @override
  _ButtonPageState createState() => _ButtonPageState();

}

class _ButtonPageState extends State<ButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button Example"),
      ),
      body:
      ListView(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  AcmPrimaryButton("Active Primary Button", onPressed: (){}),
                  Divider(color: Colors.transparent,),

                  AcmPrimaryButton("Disable Primary Button", onPressed: null),
                  Divider(color: Colors.transparent,),

                  AcmPrimaryButton("Active Primary Button Small", buttonSize: AcmSize.Small, onPressed: (){}),
                  Divider(color: Colors.transparent,),

                  AcmSecondaryButton("Active secondary button", onPressed: () {},),

                  Divider(color: Colors.transparent,),
                  AcmSecondaryButton("Disable secondary button", onPressed: null,),

                  Divider(color: Colors.transparent,),
                  AcmSecondaryButton("Active secondary button small", buttonSize: AcmSize.Small, onPressed: () {},),
                ],
              ),
            ),
          ),
        ],
      )
    );
  }
}