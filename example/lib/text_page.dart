import 'package:acm_widgets/acm_text.dart';
import 'package:flutter/material.dart';

class TextPage extends StatefulWidget {

  static const String route = "/textexample";

  @override
  _TextPageState createState() => _TextPageState();

}

class _TextPageState extends State<TextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Text Example"),
      ),
      body:
      ListView(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  AcmTitleText("Sample Title Text"),
                  Divider(),
                  AcmBodyText("Sample Short Text"),
                  Divider(),
                  AcmBodyText("Long Text, Heu, secundus tus!Ubi est albus genetrix?A falsis, hydra altus hibrida.Triticums mori, tanquam ferox contencio.Ortum satis ducunt ad regius palus.Devirginatos sunt extums de bassus hydra.Lunas manducare!"),
                  Divider(),
                  AcmBodyText("Long Text Ellipsis, Heu, secundus tus!Ubi est albus genetrix?A falsis, hydra altus hibrida.Triticums mori, tanquam ferox contencio.Ortum satis ducunt ad regius palus.Devirginatos sunt extums de bassus hydra.Lunas manducare!",
                    textOverflow: TextOverflow.ellipsis,
                    textWeight: AcmTextWeight.Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      )
    );
  }
}