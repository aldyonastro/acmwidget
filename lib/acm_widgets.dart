///ACM Widget Collection,
///
///Based on discussion with UI team :
///
library acmwidget;

import 'package:flutter/material.dart';

const primaryColors = Color(0xFFe6007d);
const fontDefaultColor = Color(0xFF333333);