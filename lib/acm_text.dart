///ACM Text Widget Collection,
///
///Based on discussion with UI team :
///
/// {@image <image alt='' src='images/AcmTextExm.png'>}
/// {@category Widget}
library text;

export 'package:acm_widgets/src/text/acm_title_text.dart';
export 'package:acm_widgets/src/text/acm_body_text.dart';

enum AcmTextWeight{
  Light,
  Normal,
  Bold
}