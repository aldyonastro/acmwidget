///ACM Button Widget Collection,
///
///Based on discussion with UI team :
///* All text in button should be in Caps
///* There is no inkwell animation when press the button
///* Width will be depand on design
///
/// {@image <image alt='' src='./images/ButtonSecondary.png'>}
/// {@category Widget}
library button;

export 'package:acm_widgets/src/button/acm_primary_button.dart';
export 'package:acm_widgets/src/button/acm_secondary_button.dart';

enum AcmSize{
  Small,
  Medium,
  Large
}