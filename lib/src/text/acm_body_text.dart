import 'package:flutter/material.dart';

import '../../acm_text.dart';

///Default Body text widget based on ACM Style
///https://app.zeplin.io/project/5ca46235eb413e0ab9bceea7/screen/5cc270bbd576fb3d99b32750
///
class AcmBodyText extends StatelessWidget {

  ///Content Text
  final String text;
  ///Text color
  final int textColor;
  ///Text size
  final double textSize;
  ///Text overflow style
  final TextOverflow textOverflow;
  ///Text Style Normal, Bold or Light
  final AcmTextWeight textWeight;
  FontWeight weight;

  ///Max line of text
  final int textMaxLine;

  AcmBodyText(this.text,{
    this.textColor = 0xFF333333,
    this.textSize = 16,
    this.textMaxLine = null,
    this.textOverflow = null,
    this.textWeight = AcmTextWeight.Normal
  });

  @override
  Widget build(BuildContext context) {

    switch(textWeight){
      case AcmTextWeight.Light: weight = FontWeight.w300;break;
      case AcmTextWeight.Normal: weight = FontWeight.normal;break;
      case AcmTextWeight.Bold: weight = FontWeight.bold;break;
      default: weight = FontWeight.normal;
    }

    return Text(
      text,
      overflow: textOverflow,
      maxLines: textMaxLine,
      style: TextStyle(
        fontFamily: 'FSAlbertPro',
        color: Color(textColor),
        fontWeight: weight,
        fontSize: textSize,
      ),
    );
  }

}