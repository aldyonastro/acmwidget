import 'package:flutter/material.dart';

import '../../acm_button.dart';

///Acm primary button,
///height of button based on [AcmSize] default is [AcmSize.Medium]
///to disable button pass null at onPressed
class AcmPrimaryButton extends StatelessWidget {

  ///Text of button
  final String text;
  ///Background color of button
  final int color;
  ///Text Color
  final int textColor;
  ///Ripple color
  final int inkWellColor;
  ///Height of button will be based on Button Size
  double height;
  ///Round size
  final double roundRadius;
  ///Border color
  final int borderColor;
  ///Border size default 2
  final double borderSize;
  ///Button disable color
  final disabledColor;
  ///Disable text color
  final disabledTextColor;
  ///disable border color
  final disableBorderColor;

  final AcmSize buttonSize;

  ///OnPress action
  final GestureTapCallback onPressed;

  bool isEnable;

  AcmPrimaryButton(this.text,{
    @required this.onPressed,
    this.buttonSize = AcmSize.Medium,
    this.color = 0xFFFF008E,
    this.textColor = 0xFFFFFFFF,
    this.borderColor = 0xFFFF008E,
    this.borderSize = 2,
    this.inkWellColor = 0xFFFF008E,
    this.disabledColor= 0xFFd8d8d8,
    this.disabledTextColor = 0xFFFFFFFF,
    this.disableBorderColor = 0xFFd8d8d8,
    this.height,
    this.roundRadius = 4,
  });

  @override
  Widget build(BuildContext context) {

    isEnable = onPressed != null ? true : false;

    switch(buttonSize){
      case AcmSize.Small: height = 32.0; break;
      case AcmSize.Medium: height = 40.0; break;
      case AcmSize.Large: height = 48.0; break;
      default : height = 40.0;
    }

    return Container(
      constraints: BoxConstraints.tightFor(height: height),
      child: FlatButton(
        splashColor: Color(inkWellColor),
        textColor: Color(textColor),
        disabledColor: Color(disabledColor),
        disabledTextColor: Color(disabledTextColor),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(roundRadius),
          side: BorderSide(
            width: borderSize,
            color: isEnable ? Color(borderColor) : Color(disableBorderColor))
        ),
        color: Color(color),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Text(
            text.toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'FSAlbertPro',
              fontWeight: FontWeight.bold
            ),
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }

}